require "Megabert/Base"
require "Megabert/Hw/Base"
require "Megabert/Os/Environment"

module Megabert
        class HddBulkTester < Megabert::Base
          
                  # check all required external commands
          
                  def self.check_commands
                    messages= []
                    error=false
                    [
                      ["lvdisplay","--help"],
                      ["lvremove" ,"--help"],
                      ["lvchange" ,"--help"],
                      ["lsblk",    "--json"],
                      ["smartctl", "--help"]
                    ].each do |cmd|
                      begin
                        Megabert::Os::Environment::check_command(cmd)
                      rescue StandardError => e
                        error = true
                        messages << e.message
                      end
                    end
                    if error then
                        puts messages.join("\n")
                        exit 1
                    end
                  end

                  def self.run()

                    # todo: check if this program runs as root(has to!)
                    self.check_commands
                    begin
                      hw = Megabert::Hw::Base.new
                    rescue StandardError => e
                      puts "#{e.message}. Aborting."
                      exit 1
                    end
                    hw.delete_unused_harddisks_lvs
                    hw.hdd_test_all 

                end
        end
end
