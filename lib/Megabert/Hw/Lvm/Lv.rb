require "Megabert/Base"
require "Megabert/Popen"

module Megabert
    module Hw
        module Lvm
          class Lv  < Megabert::Base
            def initialize(lv_name)
                @lv_name    = lv_name
                @lv_devname = "/dev/mapper/#{lv_name}"
            end
            
            # get status of a logical volume - execution errors create an exception, should be catched!
            
            def is_activated()
              status, stdout, stderr = ::Megabert::Popen.run("lvdisplay #{@lv_devname}")
              if status != 0 then
                msg = "error executing lvdisplay: #{stderr}"
                @@log.erro msg
                raise StandardError.new msg
              else
                stdout.each_line do |line|
                  if line.match(/LV Status\s+available/) then
                     return true
                  end
                end
                return false
              end
            end

            # delete a logical volume - execution errors create an exception, should be catched!
            
            def delete()
              if is_activated then
                deactivate
              end
              status, stdout, stderr = ::Megabert::Popen.run("lvremove #{@lv_devname} ")
              if status != 0 then
                msg = "error executing lvremove: #{stderr}"
                @@log.erro msg
                raise StandardError.new msg
              end
            end
            
            # deactivate a logical volume - execution errors create an exception, should be catched!
           
            def deactivate()
              status, stdout, stderr = ::Megabert::Popen.run("lvchange -an #{@lv_devname}")
              if status != 0 then
                msg = "error executing lvchange: #{stderr}"
                @@log.erro msg
                raise StandardError.new msg
              end
              return true
            end
          end
      end
  end
end
