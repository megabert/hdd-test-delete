require "Megabert/Base"
require "Megabert/Hw/Harddisk/Base"
require "pp"
require "json"

module Megabert
  module Hw
    class Base < Megabert::Base
        def initialize()
          @harddisks              = Megabert::Hw::Harddisk::Base::get_harddisks
          @unused_harddisks       = unused_harddisks
          @harddisks_to_test      = []
          @dmesg_events_processed = {}

        end
        def harddisks
          return @harddisks
        end
        def unused_harddisks
          disks = []
          harddisks.each do |hdd| 
            disks << hdd unless hdd.mounted 
          end
          @@log.dbg2 "Count of unused hard disks detected: #{disks.length}"
          return disks
        end
        def unused_harddisks_hash
          hash = {}
          unused_harddisks.each do |hdd|
            hash[hdd.name.to_sym] = true
          end
          return hash
        end
        def delete_unused_harddisks_lvs 
          begin 
            unused_harddisks.each do |hdd|
              hdd.delete_lvs
            end
          rescue StandardError => e
            @@log.erro "#{e.message}. Aborting"
          end
        end
        def detect_dmesg_hdd_connects
            hdd_connects = {}
            @@log.dbg2 "fetching dmesg data"
            status, stdout, stderr = ::Megabert::Popen.run("dmesg")

            if status != 0 then
              @@log.warn "Error calling dmesg: #{stderr}"
            end
            stdout.each_line do |line|
              matches = line.match(/^\s*\[\s*([0-9]+\.[0-9]+)\].*sd.*\[([a-z0-9]+)\].*ttached.*disk/)
              if matches then
                hdd_connects[matches[2].to_sym] = matches[1]
                  @@log.dbg2 "found attach event at #{matches[1]} for disk #{matches[2]}"
              end
            end
            return hdd_connects
        end

        def process_dmesg_connects

          # todo: improvement: save already processed dmesg lines(by time index) and not process them again
          #                    currently the dmesg log is processed in full every time
          
          # mark all newly connected and unused(e. g. unmounted) disks as not tested
          unused_hash = unused_harddisks_hash
          detect_dmesg_hdd_connects.each do |evt| 
            cur_hard_disk       = evt[0].to_s
            cur_event_timeindex = evt[1]
          if @dmesg_events_processed.has_key?(evt[1]) then
              @@log.dbg2 "dmesg line timeindex #{cur_event_timeindex} had already been processed"
            else
              if unused_hash.has_key?(cur_hard_disk) then
                @@log.dbg2 "disk #{cur_hard_disk} for connect event #{cur_event_timeindex} is unused"

                # mark disk as to be tested
                @@log.dbg2 "marking disk #{cur_hard_disk} as to_be_tested"
                new_disk = Megabert::Hw::Harddisk::Base.new(
                             Megabert::Hw::Harddisk::Base::get_lsblk_for_disk(cur_hard_disk)
                                    )
                @harddisks_to_test << new_disk
              end

              # mark the dmesg line as processed
              @@log.dbg2 "marking dmesg line timeindex #{evt[1]} as processed"
              @dmesg_events_processed[evt[1]] = true
            end
          end
          
        end

        def hdd_test_all

          # at first test all unused_harddisks
          @harddisks_to_test += @unused_harddisks

          while true do
            @@log.dbg1 "checking dmesg"
            # at second test all unused harddisks which had been newly connected
            process_dmesg_connects

            @harddisks_to_test.each{ |hdd|

              if !hdd.test_running? then
                @@log.dbg1 "starting to test hdd #{hdd.name}"
                hdd.start_test
              end
          }
          sleep 10
          end
        end
    end
  end
end
