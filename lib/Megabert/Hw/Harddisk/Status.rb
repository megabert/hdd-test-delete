require "Megabert/Base"
require "json"

module Megabert
    module Hw
        module Harddisk
          class Status < Megabert::Base
            def initialize(hdd_object)
              @hdd          = hdd_object
              @pid          = nil
              @status_file  = "#{$conf.directories.status_dir}/#{@hdd.name}.json" 
            end
            def set(state_text)
              @state        = state_text
              @last_update  = Time.now
              save
            end
            def save()
              data = {
                  :status       => @state,
                  :pid          => @pid,
                  :last_update  => @last_update
              }
              # atomic update!
              File.write(@status_file+".tmp",JSON.pretty_generate(data))
              File.rename(@status_file+".tmp",@status_file)
            end
          end
        end
    end
end

