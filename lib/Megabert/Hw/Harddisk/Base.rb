require "Megabert/Base"
require "Megabert/Popen"
require "Megabert/Os/Environment"
require "Megabert/Hw/Lvm/Lv"
require "Megabert/Hw/Harddisk/Status"
require "pp"
require "json"

module Megabert
    module Hw
        module Harddisk 
          class Base < Megabert::Base

          attr_reader :name, :mounted, :active_lv_names
            
          # list all harddisks in the system
          
          def self.get_harddisks

            harddisks = []

            status, stdout, stderr = ::Megabert::Popen.run("lsblk --json")
            if status != 0 then
              @@log.warn "lsblk exited with error"
              @@log.warn "lsblk stdout: #{stdout}"
              @@log.warn "lsblk stderr: #{stderr}"
              return false
            end
            begin
              lsblk_data = JSON.parse(stdout)
            rescue StandardError => e
              @@log.warn "lsblk JSON-output could not be parsed: #{e.message}"
              return false
            end

            if lsblk_data.has_key?("blockdevices") then
              lsblk_data["blockdevices"].each do |hdd|
                harddisks << self.new(hdd)
              end
              return harddisks
            else 
              @@log.dbg1 "lsblk found no devices"
            end
          end

          # get lsblk json data for a specific hard disk

          def self.get_lsblk_for_disk(devname)

            # devname: relative device file for a disk. e. g. sda

            status, stdout, stderr = ::Megabert::Popen.run("lsblk --json /dev/#{devname}")
            if status != 0 then
              @@log.warn "lsblk exited with error"
              @@log.warn "lsblk stdout: #{stdout}"
              @@log.warn "lsblk stderr: #{stderr}"
              return false
            end
            begin
              lsblk_data = JSON.parse(stdout)
            rescue StandardError => e
              @@log.warn "lsblk JSON-output could not be parsed: #{e.message}"
              return false
            end
            return lsblk_data["blockdevices"][0]
          end

          # list all unmounted harddisks 

          def initialize(lsblk_json)

              @@log.dbg1 "new hdd found: #{lsblk_json["name"]}"
              @name = lsblk_json["name"]
              @devname = "/dev/#{lsblk_json["name"]}"
              @mounted=false
              @active_lv_names=[]
              @smart_enabled = smart_is_enabled?
              # todo: check whether the disk generally supports smart

              # a disk has state mounted if either the while disk or one of its partitions 
              # is mounted
              
              @mounted=true if lsblk_json.has_key?("mountpoint") && lsblk_json["mountpoint"]
              unless @mounted then
                if lsblk_json.has_key?("children") then
                  lsblk_json["children"].each do |child|

                    if child["type"] == "part" then
                      if child.has_key?("mountpoint") && child["mountpoint"] then
                        @mounted=true
                      end

                      # if a partition has children those should be active lvm logical volumes
                      # inactive logical volumes would not be shown
                      if child.has_key?("children") then
                          child["children"].each do |lv|
                              @active_lv_names << lv["name"] if lv["type"] == "lvm" 
                              # lsblk show also if the lvm is mounted or not
                              if lv.has_key?("mountpoint") && lv["mountpoint"] then
                                @mounted=true
                              end
                          end
                      end
                    end

                    # an lvm physical volume is located as direct children of the disk
                    if child.has_key?("type") && child["type"] == "lvm" then
                              @active_lv_names << child["name"]
                              if child.has_key?("mountpoint") && child["mountpoint"] then
                                @mounted=true
                              end
                    end
                  end
                end
              end
              @@log.dbg1 "new hdd #{@name} mount_status: #{@mounted.to_s}"
          end

          # delete all logical volumes on the current hard disk
          
          def delete_lvs
            @active_lv_names.each do |lv_name|
              @@log.info "deleting lv #{lv_name} of hard disk #{@name}"
              lv = Megabert::Hw::Lvm::Lv.new lv_name
              begin
                lv.delete
              rescue StandardError => e
                @@log.erro "error deleting lv #{lv_name}: #{e.message}"
              end
            end
          end

          def smart_is_enabled?
              status, stdout, stderr = ::Megabert::Popen.run("smartctl -i #{@devname}")
              res=false

              if status != 0 then
                @@log.warn "smartctl exited with error"
                @@log.warn "smartctl stdout: #{stdout}"
                @@log.warn "smartctl stderr: #{stderr}"
              else
                res=true  if stdout.match(/SMART support is: Enabled/)
              end
              @@log.dbg1 "smart is #{ res ? "enabled" : "disabled" } on #{@name}"
              return res
          end
          def test_running?
            # todo: implement function
            return false
          end
          def needs_testing?
            # todo: implement function
            return true
          end
          def start_test
            status=Megabert::Hw::Harddisk::Status.new(self)
            status.set("preparing")
            status, stdout, stderr = ::Megabert::Popen.run("hdd_test #{name}")

            
          end
        end
    end
end
end

