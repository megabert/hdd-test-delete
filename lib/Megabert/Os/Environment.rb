require "Megabert/Base"
require "Megabert/Popen"

module Megabert
    module Os
        class Environment < Megabert::Base

          def self.check_command(command_set)

            # first, check if which returns a location for the given command
            status, stdout, stderr = ::Megabert::Popen.run("which #{command_set[0]}")
            raise StandardError.new "Required Command '#{command_set[0]}' not found" if status != 0

            # second, run the full command and return success if it runs successfully 
            status, stdout, stderr = ::Megabert::Popen.run(command_set.join(" "))
            raise StandardError.new "Required Command '#{command_set.join(" ")}' not supported by executable" if status != 0
          end

        end
    end
end

