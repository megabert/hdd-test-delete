require "Megabert/Base"
require "open4"

module Megabert
        class Popen < Megabert::Base
                def self.run(command,input_handle=nil)
                        @@log.dbg2(command)
                        pid, stdin, stdout, stderr = Open4::popen4 command
                        @@log.dbg2 "command opened"

                        if(input_handle) then
                                stdin.puts input.handle.read
                        end
                        stdin.close
                        stdout_text = stdout.read.strip
                        stderr_text = stderr.read.strip

                        ignore, status = Process::waitpid2 pid
                        return status.exitstatus,stdout_text,stderr_text
                end
        end
end

