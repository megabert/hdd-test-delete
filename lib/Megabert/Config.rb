require "Megabert/Base"
require "json"
require "inifile"

module Megabert
    class Config 
            def initialize(filename=nil)
              @file = filename || $config_file
              @ini = IniFile.load(@file)
            end
            def get(section,key)
              return @ini[section][key]
            end
            def write()
              ini.save
            end
            def method_missing(name, *args, &block)
              if @ini.has_section?(name) then
                return ConfigObject.new(@ini,name)
              end
            end
  end
  class ConfigObject
    def initialize(ini_object, section_name)
      @ini      = ini_object
      @section  = section_name
    end
    def method_missing(name, *args, &block)
      return @ini[@section.to_s][name.to_s]
    end
  end
end
