module Megabert
        class Log

                @log_levels = {
                                "pnic" => 1,
                                "crit" => 2,
                                "erro" => 3,
                                "warn" => 4,
                                "info" => 5,
                                "dbg1" => 6,
                                "dbg2" => 7,
                                "dbg3" => 8
                }

                def self.write(level,msg)


                        log_level = $global_log_level ? $global_log_level : "info"

                        if( @log_levels[level.to_s] <= @log_levels[log_level.to_s]) then
                                my_time = Time.now.strftime('%Y-%m-%d %H:%M:%S.%3N')
                                puts "#{my_time} : #{level.upcase} : #{msg}"
                        end
                end

                def self.dbg3(msg)      write("#{__method__}",msg) end
                def self.dbg2(msg)      write("#{__method__}",msg) end
                def self.dbg1(msg)      write("#{__method__}",msg) end
                def self.info(msg)      write("#{__method__}",msg) end
                def self.warn(msg)      write("#{__method__}",msg) end
                def self.erro(msg)      write("#{__method__}",msg) end
                def self.crit(msg)      write("#{__method__}",msg) end
                def self.pnic(msg)      write("#{__method__}",msg) end

        end
end

