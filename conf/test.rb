#!/usr/bin/env ruby

require 'inifile'

# Open and read the file
ini = IniFile.load('hdd.ini')

# Read its current contents
puts ini['directories']['status_dir']

# Edit the contents
ini['directories']['temp_dir'] = '/tmp'

# Save it back to disk
# You don't need to provide the filename, it remembers the original name
ini.save
